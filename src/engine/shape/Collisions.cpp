/*
 * Random Rogue an open source platform rogue like
 *
 * Copyright (C) 2017 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../../logging/Loggers.hpp"

#include "Collisions.hpp"

namespace rgrogue {

//------------------------------------------------------------------------------
bool Collisions::isIn(const Vector2D& point, const Polygon& polygon)
{
  unsigned int crossingSegements = 0;
  Segment rayCast(polygon.getLeftMostPoint() - Vector2D(1, 1), point);
  bool colinearSegmentFound = false;

  while(true)
  {
    for(const auto& s: polygon.getSegments())
    {
      // Raycast with colinear segment detects 3 crossing segments that is not
      // what we want. Choose another raycast and try again!
      if(rayCast.orientation(s.getP1()) == 0 &&
          rayCast.orientation(s.getP2()) == 0)
      {
        rayCast = Segment(rayCast.getP1() + Vector2D(-1, 0), point);
        crossingSegements = 0;
        colinearSegmentFound = true;

        break;
      }
      else if(rayCast.intesects(s))
        crossingSegements++;
    }

    // Retry with the new raycast
    if(colinearSegmentFound)
    {
      colinearSegmentFound = false;
      continue;
    }

    break;
  }

  return crossingSegements &= 1; // %2 == 0
}

//------------------------------------------------------------------------------
bool Collisions::isIn(const Vector2D& point, const Circle& circle)
{
  double distance = point.distance(circle.getCenter());

  return distance <= circle.getRadius();
}

//------------------------------------------------------------------------------
bool Collisions::overlaps(const Polygon& polygon1, const Polygon& polygon2)
{
  for(auto& segP1: polygon1.getSegments())
    for(auto& segP2: polygon2.getSegments())
      if(segP1.intesects(segP2))
        return true;

  // If no segment intersect, might be inside of each other
  if(isIn(polygon1.getPoints()[0], polygon2) ||
      isIn(polygon2.getPoints()[0], polygon1))
    return true;

  return false;
}

//------------------------------------------------------------------------------
bool Collisions::overlaps(const Polygon& polygon, const Circle& circle)
{
  for(auto& s: polygon.getSegments())
  {
    Segment ac(s.getP1(), circle.getCenter());
    Vector2D vAc = ac.toVector();
    Vector2D vAb = s.toVector();
    Vector2D projAcAb = vAc.projectionOn(vAb);
    Segment ad(s.getP1(), s.getP1() + projAcAb);
    float adLength = ad.toVector().length();

    /*
     * A * Segment(A,B)
     *    \
     *     \     * C (circle center)
     *    D *
     *       \
     *        \
     *         * B
     *
     * AD being the projection AC on AB
     */

    // Projection bigger than segment
    if(adLength >= vAb.length())
    {
      // FIXME: Test case!?
      if(isIn(s.getP2(), circle))// || isIn(s.getP1(), circle))
        return true;
    }
    else if(adLength <= circle.getRadius())
      return true;
  }

  return false;
}

}       // namespace
