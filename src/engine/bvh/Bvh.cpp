/*
 * Random Rogue an open source platform rogue like
 *
 * Copyright (C) 2017 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cassert>

#include "../shape/Collisions.hpp"
#include "Bvh.hpp"

namespace rgrogue {

//------------------------------------------------------------------------------
Bvh::Bvh()
{
}

//------------------------------------------------------------------------------
Bvh::~Bvh()
{
}

//------------------------------------------------------------------------------
void Bvh::addObject(std::unique_ptr<IWorldObject>& object)
{
  m_objects.push_back(std::move(object));
}

//------------------------------------------------------------------------------
void Bvh::getObjects(const Rectangle& boundingBox,
      std::list<std::reference_wrapper<IWorldObject> >& objects)
{
  objects.clear();

  // Planning to implement a balanced search tree ...
  for(auto& object: m_objects)
  {
    IShape& s = (*object).getShape();

    switch(s.getShapeType())
    {
    case ShapeType::POLYGON:
    {
      Polygon& crtPolygon = static_cast<Polygon&>(s);

      if(Collisions::overlaps(boundingBox, crtPolygon))
        objects.push_back(*object);
    }
    break;
    case ShapeType::CIRCLE:
      // TODO
    default:
      assert(0);
    }
  }
}
/* As reminder
//------------------------------------------------------------------------------
void Polygon::getBoundingBox(std::unique_ptr<IShape>& bb) const
{
  float maxX = m_points[0].getX();
  float minX = m_points[0].getX();
  float maxY = m_points[0].getY();
  float minY= m_points[0].getY();

  for(unsigned int i = 1 ; i < m_points.size() ; ++i)
  {
    if(m_points[i].getX() > maxX)
      maxX = m_points[i].getX();
    if(m_points[i].getX() < minX)
      minX = m_points[i].getX();
    if(m_points[i].getY() > maxY)
      maxY = m_points[i].getY();
    if(m_points[i].getY() < minY)
      minY = m_points[i].getY();
  }

  bb.reset(new Polygon({
      Vector2D(minX, maxY),
      Vector2D(maxX, maxY),
      Vector2D(maxX, minY),
      Vector2D(minX, minY)}));
}

//------------------------------------------------------------------------------
void Circle::getBoundingBox(std::unique_ptr<IShape>& bb) const
{
  bb.reset(new Polygon({
    getCenter() + Vector2D(m_radius, -m_radius),
    getCenter() + Vector2D(m_radius, m_radius),
    getCenter() + Vector2D(-m_radius, m_radius),
    getCenter() + Vector2D(-m_radius, -m_radius)
  }));
}
 */

}       // namespace
