/*
 * Random Rogue an open source platform rogue like
 *
 * Copyright (C) 2017 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LoggerNull.hpp"

namespace rgrogue {

//------------------------------------------------------------------------------
LoggerNull::LoggerNull()
{
}

//------------------------------------------------------------------------------
LoggerNull::~LoggerNull()
{
}

//------------------------------------------------------------------------------
int LoggerNull::init()
{
  return 0;
}

//------------------------------------------------------------------------------
ILogger* LoggerNull::clone(const std::string& name)
{
  return new LoggerNull();
}

//------------------------------------------------------------------------------
void LoggerNull::setMaxLogLevel(LogLevel logLevel)
{
}

//------------------------------------------------------------------------------
LogLevel LoggerNull::getMaxLogLevel()
{
  return LogLevel::DEBUG;
}

//------------------------------------------------------------------------------
LogFormatter LoggerNull::getFormater(LogLevel logLevel)
{
  return LogFormatter(*this);
}

//------------------------------------------------------------------------------
void LoggerNull::sinkLogLine(LogLevel logLevel, const std::string& message)
{
}

}       // namespace
