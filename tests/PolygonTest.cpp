/*
 * Random Rogue an open source platform rogue like
 *
 * Copyright (C) 2017 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/engine/shape/Polygon.hpp"

#include <CppUTest/TestHarness.h>

using namespace rgrogue;

//------------------------------------------------------------------------------
TEST_GROUP(Polygon)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(Polygon, getShapeType)
{
  Polygon p;

  CHECK(ShapeType::POLYGON == p.getShapeType());
}

//------------------------------------------------------------------------------
TEST(Polygon, getSegmentsFromTriangle)
{
  Vector2D p1(0, 0);
  Vector2D p2(3, 5);
  Vector2D p3(1, 1);
  Polygon p({p1, p2, p3});
  std::vector<Segment> s = p.getSegments();

  CHECK_EQUAL(3, s.size());
  CHECK(p1 == s[0].getP1());
  CHECK(p2 == s[0].getP2());
  CHECK(p2 == s[1].getP1());
  CHECK(p3 == s[1].getP2());
  CHECK(p3 == s[2].getP1());
  CHECK(p1 == s[2].getP2());
}

//------------------------------------------------------------------------------
TEST(Polygon, getSegmentsFromLine)
{
  Vector2D p1(4, 5);
  Vector2D p2(6, 7);
  Polygon p({p1, p2});
  std::vector<Segment> s = p.getSegments();

  CHECK_EQUAL(1, s.size());
  CHECK(p1 == s[0].getP1());
  CHECK(p2 == s[0].getP2());
}

//------------------------------------------------------------------------------
TEST(Polygon, getSegmentsWithOnePoint)
{
  Vector2D p1(4, 5);
  Polygon p({p1});
  std::vector<Segment> s = p.getSegments();

  CHECK_EQUAL(1, s.size());
  CHECK(p1 == s[0].getP1());
  CHECK(p1 == s[0].getP2());
}

//------------------------------------------------------------------------------
TEST(Polygon, getSegmentsDefaultConstructor)
{
  Polygon p;
  std::vector<Segment> s = p.getSegments();

  CHECK_EQUAL(1, s.size());
  CHECK(Vector2D(0, 0) == s[0].getP1());
  CHECK(Vector2D(0, 0) == s[0].getP2());
}

//------------------------------------------------------------------------------
TEST(Polygon, constructorWithPoints)
{
  Vector2D p1(1, 1);
  Vector2D p2(3, 3);
  Polygon p({p1, p2});

  CHECK_FALSE(p.getPoints().empty());
  CHECK(p1 == p.getPoints()[0]);
  CHECK(p2 == p.getPoints()[1]);
}

//------------------------------------------------------------------------------
TEST(Polygon, defaultConstructorHasOneOrigin)
{
  Polygon p;

  CHECK_FALSE(p.getPoints().empty());
  CHECK(Vector2D(0, 0) == p.getPoints()[0]);
}

//------------------------------------------------------------------------------
TEST(Polygon, defaultConstructor)
{
  Polygon p;
}
