/*
 * Random Rogue an open source platform rogue like
 *
 * Copyright (C) 2017 R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "../src/engine/shape/Collisions.hpp"

#include <CppUTest/TestHarness.h>

using namespace rgrogue;

//------------------------------------------------------------------------------
TEST_GROUP(Collisions)
{
  TEST_SETUP()
  {
  }

  TEST_TEARDOWN()
  {
  }
};

//------------------------------------------------------------------------------
TEST(Collisions, overlapsLineProjectionBiggerThanSegmentNearP2InCircle)
{
  Polygon line({Vector2D(1, 5), Vector2D(3, 5)});
  Circle circle(Vector2D(4, 2), 4);

  CHECK(Collisions::overlaps(line, circle));
}

//------------------------------------------------------------------------------
TEST(Collisions, overlapsLineCenterColinearP1InCircle)
{
  Polygon line({Vector2D(3, 3), Vector2D(5, 5)});
  Circle circle(Vector2D(2, 2), 4);

  CHECK(Collisions::overlaps(line, circle));
}

//------------------------------------------------------------------------------
TEST(Collisions, lineOverlapsCircle)
{
  Polygon line({Vector2D(3, 3), Vector2D(5, 5)});
  Circle circle(Vector2D(5, 4), 3);

  CHECK(Collisions::overlaps(line, circle));
}

//------------------------------------------------------------------------------
TEST(Collisions, lineDoesntOverlaCircle)
{;
  Polygon line({Vector2D(-5, 0), Vector2D(-2, -2)});
  Circle circle(Vector2D(5, 5), 3);

  CHECK_FALSE(Collisions::overlaps(line, circle));
}

//------------------------------------------------------------------------------
TEST(Collisions, isInPointInCircleIn)
{
  Vector2D point(1, 1);
  Circle circle(Vector2D(5, 5), 10);

  CHECK(Collisions::isIn(point, circle));
}

//------------------------------------------------------------------------------
TEST(Collisions, isInPointInCircleNotIn)
{;
  Vector2D point(1, 1);
  Circle circle(Vector2D(5, 5), 1);

  CHECK_FALSE(Collisions::isIn(point, circle));
}

//------------------------------------------------------------------------------
TEST(Collisions, overlapsSecondTriangleInsideFirst)
{
  Vector2D t1p1(-10, 0);
  Vector2D t1p2(10, 100);
  Vector2D t1p3(30, 0);
  Polygon t1({t1p1, t1p2, t1p3});
  Vector2D t2p1(5, 1);
  Vector2D t2p2(6, 2);
  Vector2D t2p3(7, 1);
  Polygon t2({t2p1, t2p2, t2p3});

  CHECK(Collisions::overlaps(t1, t2));
}

//------------------------------------------------------------------------------
TEST(Collisions, overlapsFirstTriangleInsideSecond)
{
  Vector2D t1p1(5, 1);
  Vector2D t1p2(6, 2);
  Vector2D t1p3(7, 1);
  Polygon t1({t1p1, t1p2, t1p3});
  Vector2D t2p1(-10, 0);
  Vector2D t2p2(10, 100);
  Vector2D t2p3(30, 0);
  Polygon t2({t2p1, t2p2, t2p3});

  CHECK(Collisions::overlaps(t1, t2));
}

//------------------------------------------------------------------------------
TEST(Collisions, overlapsTwoTrianglesWithColinearRayCastDontOverlap)
{
  Vector2D t1p1(1, 1);
  Vector2D t1p2(1, 5);
  Vector2D t1p3(2, 2);
  Polygon t1({t1p1, t1p2, t1p3});
  Vector2D t2p1(3, 3);
  Vector2D t2p2(3, 10);
  Vector2D t2p3(4, 4);
  Polygon t2({t2p1, t2p2, t2p3});

  CHECK_FALSE(Collisions::overlaps(t1, t2));
}

//------------------------------------------------------------------------------
TEST(Collisions, overlapsTwoTrianglesOverlap)
{
  Vector2D t1p1(0, 0);
  Vector2D t1p2(3, 5);
  Vector2D t1p3(5, 0);
  Polygon t1({t1p1, t1p2, t1p3});
  Vector2D t2p1(3, 1);
  Vector2D t2p2(5, 6);
  Vector2D t2p3(8, 1);
  Polygon t2({t2p1, t2p2, t2p3});

  CHECK(Collisions::overlaps(t1, t2));
}

//------------------------------------------------------------------------------
TEST(Collisions, overlapsTwoTrianglesDont)
{
  Vector2D t1p1(0, 0);
  Vector2D t1p2(3, 5);
  Vector2D t1p3(5, 0);
  Polygon t1({t1p1, t1p2, t1p3});
  Vector2D t2p1(100, 1);
  Vector2D t2p2(110, 11);
  Vector2D t2p3(120, 1);
  Polygon t2({t2p1, t2p2, t2p3});

  CHECK_FALSE(Collisions::overlaps(t1, t2));
}

//------------------------------------------------------------------------------
TEST(Collisions, isInPointInTriangle)
{
  Vector2D p1(0, 0);
  Vector2D p2(3, 5);
  Vector2D p3(5, 0);
  Vector2D point(3, 2);
  Polygon triangle({p1, p2, p3});

  CHECK(Collisions::isIn(point, triangle));
}

//------------------------------------------------------------------------------
TEST(Collisions, isInPointNotInTriangle)
{
  Vector2D p1(0, 0);
  Vector2D p2(10, 10);
  Vector2D p3(20, 0);
  Vector2D point(-5, -5);
  Polygon triangle({p1, p2, p3});

  CHECK_FALSE(Collisions::isIn(point, triangle));
}
